<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\PostCategory;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('posts.index')->with(['posts' => $posts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create', ['categories' => Category::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, Post::$rules);
        $image = $request->image->store('images/posts');
        $data['is_active'] = $request->is_active ? 1 : 0;
        $data['is_featured'] = $request->is_featured ? 1 : 0;
        $data['body'] = $request->body;
        $data['description'] = $request->description;
        $data['meta_description'] = $request->meta_description;
        $data['meta_keywords'] = $request->meta_keywords;
        $data['slug'] = SlugService::createSlug(Post::class, 'slug', $request->title);
        $data['title'] = $request->title;
        $data['post_id'] = $request->post_id;
        $data['image'] = $image;
        $categories = $request->categories;
        if($post = Post::create($data)) {
            $post->categories()->sync($categories);
        }
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        return view('posts.edit')->with(['post' => $post, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, Post::$update_rules);
        if ($request->hasFile('image')) {
            $image = $request->image->store('images/posts');
            $data['image'] = $image;
        } else {
            $data['image'] = $post->image;
        }
        $data['is_active'] = $request->is_active ? 1 : $post->is_active;
        $data['is_featured'] = $request->is_featured ? 1 : $post->is_featured;
        $data['body'] = $request->body ?? $post->body;
        $data['description'] = $request->description ?? $post->description;
        $data['meta_description'] = $request->meta_description ?? $post->meta_description;
        $data['meta_keywords'] = $request->meta_keywords ?? $post->meta_keywords;
        if ($request->title != $post->title) {
            $post->slug = null;
            $data['slug'] = SlugService::createSlug(Post::class, 'slug', $request->title);
        } else {
            $data['slug'] = $post->slug;
        }
        $data['title'] = $request->title ?? $post->title;

        if($post->update($data)) {
            $post->categories()->sync($request->categories);
        }
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return void
     */
    public function destroy(Post $post)
    {
        //
    }

    public function trash() {
        //
        $posts = Post::onlyTrashed()->get();
        return view('posts.trash', ['posts' => $posts]);
    }

    public function emptyTrash() {
        Post::onlyTrashed()->forceDelete();
        return redirect()->route('posts.index');
    }
}
