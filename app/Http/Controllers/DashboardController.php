<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function dashboard() {
        return view('home');
    }

    public function delete_record( Request $request ) {
        $model = "App\\". $request->model;
        $id = $request->id;
        $result = $model::destroy($id);
        if ($result) {
            return response()->json(['message' => '<i class="fa fa-check-square-o"></i> Successfully Deleted.'], 232);
        } else {
            return response()->json(['message' => '<i class="fa fa-exclamation-triangle"></i> Something went wrong and nothing Deleted.'], 419);
        }
    }

    public function restore_record( Request $request ) {
        $model = "App\\". $request->model;
        $id = $request->id;
        $result = $model::onlyTrashed()->find($id)->restore();
        if ($result) {
            return response()->json(['message' => '<i class="fa fa-check-square-o"></i> Successfully Restored.'], 232);
        } else {
            return response()->json(['message' => '<i class="fa fa-exclamation-triangle"></i> Something went wrong and nothing Restored.'], 419);
        }
    }

    public function empty_records( Request $request ) {
        $model = "App\\". $request->model;
        $result = $model::onlyTrashed()->forceDelete();
        if ($result) {
            return response()->json(['message' => '<i class="fa fa-check-square-o"></i> Successfully empty.'], 232);
        } else {
            return response()->json(['message' => '<i class="fa fa-exclamation-triangle"></i> Something went wrong and nothing happen.'], 419);
        }
    }

    public function logout() {
        Auth::logout();
    }
}
