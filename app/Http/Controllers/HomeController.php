<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featured_posts = Post::where(['is_featured' => 1, 'is_active' => 1])->take(3)->get();
        $posts = Post::where(['is_active' => 1])->take(6)->get();
        $categories = Category::all();
        return view('index', ['categories' => $categories, 'posts' => $posts, 'featured_posts' => $featured_posts]);
    }

    public function post_view($slug) {
        $categories = Category::all();
        $post = Post::where(['slug' => $slug, 'is_active' => 1])->firstOrFail();
        return view('posts.view')->with(['post' => $post, 'categories' => $categories]);
    }

    public function category_view($id) {
        $categories = Category::all();
        $category = Category::where(['id' => $id])->first();
        return view('categories.view')->with(['categories' => $categories, 'category' => $category]);

    }
}
