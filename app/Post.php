<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $fillable = ['title','slug','description','body','image','category_id','is_active','is_featured','meta_description','meta_keywords','published_at'];
    protected $dates = ['deleted_at', 'published_at'];

    public static $rules = [
        'title'       => 'required',
        'description'=>'required',
        'body'=>'required',
        'image' => 'required|image|dimensions:width=1920,height=1280',

    ];
    public static $update_rules = [
        'title' => 'required',
        'description'=>'required',
        'body'=>'required',
        'image' => 'nullable|image|dimensions:width=1920,height=1280',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function categories() {
        return $this->belongsToMany(Category::class);
    }
}

