<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Category extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'image'];
    protected $dates = ['deleted_at'];
    public static $rules = [
        'title' => 'required',
        'image' => 'required|image|dimensions:width=1920,height=1280'
    ];
    public static $update_rules = [
        'title' => 'required',
        'image' => 'nullable|image|dimensions:width=1920,height=1280'
    ];

    public function posts() {
        return $this->belongsToMany(Post::class);
    }

}
