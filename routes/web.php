<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/post/{slug}', 'HomeController@post_view')->name('post-view');
Route::get('/category/{id}', 'HomeController@category_view')->name('category-view');
Route::get('/admin', 'DashboardController@dashboard')->name('home');
Route::post('delete-record',array('as' => "delete-record",'uses'=> "DashboardController@delete_record"));
Route::post('restore-record',array('as' => "restore-record",'uses'=> "DashboardController@restore_record"));
Route::post('empty-records',array('as' => "empty-records",'uses'=> "DashboardController@empty_records"));
Route::prefix('admin')->middleware('auth')->group(function() {
    Route::get('/categories/trash', 'CategoryController@trash');
    Route::resource('categories', 'CategoryController');
    Route::get('/posts/trash', 'PostController@trash');
    Route::resource('posts', 'PostController');
});
