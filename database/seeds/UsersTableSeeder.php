<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::insert([
            'name' => 'Alaa M. Jaddou',
            'email' => 'alaamjaddou@gmail.com',
            'password' => bcrypt('123456')
        ]);
    }
}
