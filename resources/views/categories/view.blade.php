<?php
/**
 * Created by PhpStorm.
 * User: Alaa M. Jaddou
 * Date: 10/24/2018
 * Time: 10:39 PM
 */
?>
@extends('app')

@section('content')
<!-- Page Content -->
<div class="container mt-5">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="my-4">
                {{ $category->title }}
            </h1>

            <!-- Blog Post -->
            @foreach($category->posts as $post)
                <div class="card mb-4">
                    <img class="card-img-top" src="{{ asset('storage/'.$post->image) }}" alt="{{ $post->title }}">
                    <div class="card-body">
                        <h2 class="card-title">{{ $post->title }}</h2>
                        <p class="card-text">{{ $post->description }}</p>
                        <a href="{{ route('post-view', $post->slug) }}" class="btn btn-primary">Read More &rarr;</a>
                    </div>
                    <div class="card-footer text-muted">
                        Posted on {{ $post->created_at->format("F jS, Y") }}
                    </div>
                </div>
            @endforeach

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                @foreach($categories as $cat)
                                    <li>
                                        <a href="{{ route('category-view', $cat->id) }}">{{ $cat->title }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
@endsection