<?php
/**
 * Created by PhpStorm.
 * User: AlaaM.Jaddou
 * Date: 10/24/2018
 * Time: 4:00 PM
 */

?>
@extends('app')

@section('title', 'Home Page')

@section('css')
    <style>
        .carousel-item img {
            max-height: 100vh;
            object-fit: cover;
        }
        .carousel-caption {
            background-color: rgba(0, 0, 0, 0.7);
        }

        /* The actual timeline (the vertical ruler) */
        .timeline {
            background: #cccccc;
            position: relative;
            max-width: 100%;
            margin: 0 auto;
        }

        /* The actual timeline (the vertical ruler) */
        .timeline::after {
            content: '';
            position: absolute;
            width: 6px;
            background-color: white;
            top: 0;
            bottom: 0;
            left: 50%;
            margin-left: -3px;
        }

        /* Container around content */
        .timeline .container {
            padding: 10px 40px;
            position: relative;
            background-color: inherit;
            width: 50%;
            margin: unset;
        }

        /* The circles on the timeline */
        .timeline .container::after {
            content: '';
            position: absolute;
            width: 25px;
            height: 25px;
            right: -13px;
            background-color: white;
            border: 4px solid #FF9F55;
            top: 15px;
            border-radius: 50%;
            z-index: 1;
        }

        /* Place the container to the left */
        .left {
            left: 0;
        }

        /* Place the container to the right */
        .right {
            left: 50%;
        }

        /* Add arrows to the left container (pointing right) */
        .left::before {
            content: " ";
            height: 0;
            position: absolute;
            top: 22px;
            width: 0;
            z-index: 1;
            right: 30px;
            border: medium solid white;
            border-width: 10px 0 10px 10px;
            border-color: transparent transparent transparent white;
        }

        /* Add arrows to the right container (pointing left) */
        .right::before {
            content: " ";
            height: 0;
            position: absolute;
            top: 22px;
            width: 0;
            z-index: 1;
            left: 30px;
            border: medium solid white;
            border-width: 10px 10px 10px 0;
            border-color: transparent white transparent transparent;
        }

        /* Fix the circle for containers on the right side */
        .right::after {
            left: -13px;
        }

        /* The actual content */
        .content {
            padding: 20px 30px;
            background-color: white;
            position: relative;
            border-radius: 6px;
        }

        /* Media queries - Responsive timeline on screens less than 600px wide */
        @media screen and (max-width: 600px) {
            /* Place the timelime to the left */
            .timeline::after {
                left: 31px;
            }

            /* Full-width containers */
            .timeline .container {
                width: 100%;
                padding-left: 70px;
                padding-right: 25px;
            }

            /* Make sure that all arrows are pointing leftwards */
            .timeline .container::before {
                left: 60px;
                border: medium solid white;
                border-width: 5px 5px 5px 0;
                border-color: transparent white transparent transparent;
            }

            /* Make sure all circles are at the same spot */
            .left::after, .right::after {
                left: 15px;
            }

            /* Make all right containers behave like the left ones */
            .right {
                left: 0%;
            }
        }
        .hashtag {
            color: #1c94e0;
            text-decoration: none;
            font-size: 14px;
            line-height: 20px;
            cursor: pointer;
        }

    </style>
@endsection

@section('content')
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($featured_posts as $post)
                <div class="carousel-item @if($loop->first) active @endif">
                    <img class="d-block w-100" src="{{ asset('storage/' . $post->image) }}" alt="{{ $post->title }}">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>{{ $post->title }}</h5>
                        <p>{{ $post->description }}</p>
                    </div>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <h1 class="my-3 text-center">
        Latest Posts
    </h1>
    <div class="container">
        <div class="row my-3">
            @foreach($posts as $key => $record)
                @if($key % 3 == 0)
                    </div><div class="row my-3">
                @endif
                <div class=" col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="card-header">
                            <h2>{{ str_limit($record->title, 15, '') }}</h2>
                        </div>
                        <div class="card-img">
                            <img src="{{ asset('storage/' . $record->image) }}" class="img-fluid" />
                        </div>
                        <div class="card-body">
                            <p>{{ str_limit($record->description, 200) }}</p>
                            @foreach($record->categories as $category)
                                <a href="{{ route('category-view', $category->id) }}" class="hashtag">#{{ snake_case($category->title) }}</a>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <a href="{{ route('post-view', $record->slug) }}" class="btn btn-secondary btn-outline-secondary btn-block">
                                Read more
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
   @endsection

@section('js')
@endsection
