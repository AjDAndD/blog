<?php
/**
 * Created by PhpStorm.
 * User: Alaa M. Jaddou
 * Date: 10/24/2018
 * Time: 10:56 PM
 */
?>
@extends('app')
@section('content')
<!-- Page Content -->
<div class="container mt-5">

    <div class="row">

        <!-- Post Content Column -->
        <div class="col-lg-9">
            <!-- Title -->
            <h1 class="mt-4">{{ $post->title }}</h1>
            <hr>
            <!-- Date/Time -->
            <p>Posted on {{ $post->created_at->format("F jS, Y") }} at {{ $post->created_at->format("H:i a") }}</p>

            <hr>

            <!-- Preview Image -->
            <img class="img-fluid rounded" src="{{ asset('storage/' . $post->image) }}" alt="{{ $post->title }}">

            <hr>

            <!-- Post Content -->
            {!! $post->body !!}

        </div>

        <!-- Sidebar Widgets Column -->


        <!-- Sidebar Widgets Column -->
        <div class="col-md-3">

            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <ul class="list-unstyled mb-0">
                        @foreach($categories as $cat)
                            <li>
                                <a href="{{ route('category-view', $cat->id) }}">{{ $cat->title }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
@endsection