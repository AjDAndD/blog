<?php
/**
 * Created by PhpStorm.
 * User: Alaa M. Jaddou
 * Date: 10/23/2018
 * Time: 6:08 PM
 */
?>

@extends('adminlte::page')

@section('title', 'Posts')

@section('content_header')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <h1>List Posts <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right">Create</a></h1>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="alert" id="alert"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="posts-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>thump</th>
                        <th>title</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                    </tr>
                </thead>
                <tbody>
                    @if($posts->count() > 0)
                        @foreach($posts as $key => $row)
                            <tr id="row_{{ $row->id }}">
                                <td>{{ ++$key }}</td>
                                <td class="col-md-2"><img src="{{ asset('storage/' . $row->image) }}" class="img-responsive" /> </td>
                                <td>{{ $row->title }}</td>
                                <td>{{ $row->created_at->format("F jS, Y") }}</td>
                                <td>
                                    <a href="javascript:deleteModelItem('Post', '{{$row->id}}');" class="btn btn-danger mr-3"><i class="fa fa-trash"></i> </a>
                                    <a href="{{ route('posts.edit', $row->id) }}" class="btn btn-warning"><i class="fa fa-edit"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">
                                There are no records.
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
            vertical-align: middle;
        }
    </style>
@endsection

@section('js')
    <script>

        function deleteModelItem(model, id) {
            if (confirm('Delete this Record?')) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    data: {id: id, model: model},
                    url: "{{ route('delete-record') }}",
                    type: "POST",
                    success: function (data) {
                        $('#alert').addClass('alert-success').html(data.message);
                        $('#row_' + id).hide();
                    },
                    error: function (data) {
                        console.log(data);
                        $('#alert').addClass('alert-danger').html(data.responseJSON.message);
                    }
                });
            }
        }
    </script>
@endsection
