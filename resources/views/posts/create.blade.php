<?php
/**
 * Created by PhpStorm.
 * User: Alaa M. Jaddou
 * Date: 10/23/2018
 * Time: 6:08 PM
 */
?>

@extends('adminlte::page')

@section('title', 'Create Posts')

@section('content_header')
    <h1>Create Posts <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right">Create</a></h1>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="image">Post Banner</label>
                        <div class="input-group input-file" name="image">
                            <input type="text" class="form-control" placeholder='Choose a file...' />
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-choose" type="button">Choose</button>
                            </span>
                            <small id="titleHelp" class="form-text text-muted">{{ $errors->first('image') ?? 'This field required.' }}</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputTitle">Post title</label>
                        <input type="text" name="title" class="form-control" id="exampleInputTitle" aria-describedby="titleHelp" placeholder="Enter title">
                        <small id="titleHelp" class="form-text text-muted">{{ $errors->first('title') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="categories[]">Category</label>
                        <select name="categories[]" multiple data-live-search="true" class="form-control selectpicker">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                            @endforeach
                        </select>
                        <small id="category_idHelp" class="form-text text-muted">{{ $errors->first('category_id') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputDescription">Post short description</label>
                        <textarea name="description" class="form-control" id="exampleInputDescription" aria-describedby="descriptionHelp"></textarea>
                        <small id="descriptionHelp" class="form-text text-muted">{{ $errors->first('description') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputBody">Post Content</label>
                        <textarea name="body" class="form-control" id="body" aria-describedby="bodyHelp"></textarea>
                        <small id="bodyHelp" class="form-text text-muted">{{ $errors->first('body') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputDescription">Post meta description</label>
                        <textarea name="meta_description" class="form-control" id="exampleInputDescription" aria-describedby="descriptionHelp"></textarea>
                        <small id="descriptionHelp" class="form-text text-muted">{{ $errors->first('meta_description') }}</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputDescription">Post meta keywords</label>
                        <textarea name="meta_keywords" class="form-control" id="exampleInputDescription" aria-describedby="descriptionHelp"></textarea>
                        <small id="descriptionHelp" class="form-text text-muted">{{ $errors->first('meta_keywords') }}</small>
                    </div>
                    <div class="form-group">
                        <div class="form-check form-check-inline">
                            <label class="form-check-label col-md-2" for="is_active">
                                <input class="form-check-input" name="is_active" type="checkbox" id="is_active"> is active
                            </label>
                            <label class="form-check-label col-md-2" for="is_featured">
                                <input class="form-check-input" name="is_featured" type="checkbox" id="is_featured"> is Published
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'body' );
    </script>
    <script>
        $(function() {
            bs_input_file();
        });

        function bs_input_file() {
            $(".input-file").before(
                function() {
                    if ( ! $(this).prev().hasClass('input-ghost') ) {
                        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                        element.attr("name",$(this).attr("name"));
                        element.change(function(){
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function(){
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function(){
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor","pointer");
                        $(this).find('input').mousedown(function() {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
    </script>
@endsection
